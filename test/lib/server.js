
let p = require('../../package.json');
let version = p.version.split('.').shift();
let server = {
  restApiRoot: 'api' + (version > 0 ? '/v' + version : ''),
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || 3000
};

let serverUrl = `http://${server.host}:${server.port}/${server.restApiRoot}/`

module.exports = { serverUrl };