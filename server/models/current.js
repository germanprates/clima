'use strict';

const test = (process.env.NODE_ENV == 'development');

module.exports = function(Current) {

  Current.on('attached', function () {

    Current.find = function (filter = {}, options, callback) {

      Current.app.models.Ipapi.getLocation(null, test, function(_err, location) {
  
        let data = {
          cityName: location.city,
          zip: location.zip,
        }
        
        Current.app.models.Weather.current(data, function(_err, weatherResponse) {

          let customResponse = {
            location: {
              country: location.country,
              countryCode: location.countryCode,
              regionName: location.regionName,
              city: location.city,
              zip: location.zip,
              timezone: location.timezone
            },
            weather: {
              main: weatherResponse.weather[0].main,
              description: weatherResponse.weather[0].description,
              temp: weatherResponse.main.temp,
              feels_like: weatherResponse.main.feels_like,
              temp_min: weatherResponse.main.temp_min,
              temp_max: weatherResponse.main.temp_max,
              visibility: weatherResponse.visibility,
              wind: weatherResponse.wind,
              clouds: weatherResponse.clouds,
            }
          };
  
          return callback(null, customResponse)
        })
  
      })
    }
  });

  
  Current.city = function (city, callback) {
    
    Current.app.models.Weather.current({ cityName: city }, function(_err, weatherResponse) {

      if (_err) {
        let error = new Error()
        if (_err && _err.status == '404' && _err.statusText == 'Not Found') {
          error.name = 'Bad Request';
          error.statusCode = 404;
          error.message = `No se encontró informació para la Ciudad "${city}"`
        } else {
          error.name = 'Internal error';
          error.statusCode = 500;
          error.message = 'Internal Server Error';          
        }
        return callback(error);

      } else {
  
        let customResponse = {
          location: { city: city },
          weather: {
            main: weatherResponse.weather[0].main,
            description: weatherResponse.weather[0].description,
            temp: weatherResponse.main.temp,
            feels_like: weatherResponse.main.feels_like,
            temp_min: weatherResponse.main.temp_min,
            temp_max: weatherResponse.main.temp_max,
            visibility: weatherResponse.visibility,
            wind: weatherResponse.wind,
            clouds: weatherResponse.clouds,
          }
        };
  
        return callback(null, customResponse)
      }
    })
  }

  Current.remoteMethod('city',
    {
      accepts: [
        { arg: 'city', type: 'String', required: false, http: { source: 'path' }}
      ],
      http: { path: '/:city', verb: 'get' },
      returns: { type: 'Object', root: true }
    }
  )

};
