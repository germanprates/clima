'use strict';

const axios = require('axios');
const CONFIG = require('../clima.config.json');
const WEATHER_API = CONFIG.services.openweathermap.api;
const WEATHER_API_KEY = CONFIG.services.openweathermap.api_key;
const DEFAULT = CONFIG.services.openweathermap.default;

module.exports = function (Weather) {

  Weather.current = function (data, callback) {
    let params = {
      cityName: data.cityName ? data.cityName : DEFAULT.cityName,
      lang: data.lang ? data.lang : DEFAULT.lang,
      units: data.units ? data.units : DEFAULT.units
    }

    let query;
    if (data.zip) {
      query = `${params.cityName},${data.zip}`
    } else {
      query = `${params.cityName}`
    }

    let endpoint = `${WEATHER_API}weather?q=${query}&appid=${WEATHER_API_KEY}&lang=${params.lang}&units=${params.units}`

    axios.get(endpoint)
      .then(response => {
        return callback(null, response.data)
      })
      .catch((error) => { 
        return callback(error.response)
      });
  }

  Weather.forecast = function (data, callback) {
    let params = {
      cityName: data.cityName ? data.cityName : DEFAULT.cityName,
      lang: data.lang ? data.lang : DEFAULT.lang,
      units: data.units ? data.units : DEFAULT.units
    }

    let query;
    if (data.zip) {
      query = `${params.cityName},${data.zip}`
    } else {
      query = `${params.cityName}`
    }

    let endpoint = `${WEATHER_API}forecast?q=${query}&appid=${WEATHER_API_KEY}&lang=${params.lang}&units=${params.units}`

    axios.get(endpoint)
      .then(response => {
        return callback(null, response.data)
      })
      .catch((error) => {
        return callback(error.response)
      });
  }

};
