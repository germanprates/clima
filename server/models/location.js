'use strict';

module.exports = function(Location) {  
  
  Location.getLocation = function (def, callback) {

    Location.app.models.Ipapi.getLocation(null, def, function(_err, res) {
      return callback(_err, res)
    })
  }

  Location.remoteMethod('getLocation',
    {
      accessType: 'EXECUTE',
      accepts: [
        { arg: 'def', type: 'Boolean', required: false, default: true }
      ],
      http: { path: '/', verb: 'get' },
      returns: { type: 'object', root: true }
    }
  );
};
