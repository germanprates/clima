'use strict';

let supertest = require('supertest');
let should = require('should');

let serverUrl = require('./lib/server').serverUrl;
let server = supertest.agent(serverUrl);
const modelName = 'location';

console.log('serverUrl', serverUrl);

describe('Testing entrypoint: /' + modelName, function () {
  it('get ' + modelName + ' should return 200 with location info', function (done) {
    server
      .get(modelName)
      .expect(200)
      .expect('Content-type', /json/)
      .end(function (err, resp) {
        if (err) {
          return done(err);
        }

        resp.body.should.be.a.Object;
        resp.body.should.have.property('status');
        resp.body.status.should.be.equal('success');

        resp.body.should.have.property('city');
        resp.body.city.should.be.not.empty;
        
        done();

      });
  });

});
