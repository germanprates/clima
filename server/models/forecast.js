'use strict';
const test = (process.env.NODE_ENV == 'development');

module.exports = function(Forecast) {

  Forecast.on('attached', function () {

    Forecast.find = function (filter = {}, options, callback) {

      Forecast.app.models.Ipapi.getLocation(null, !test, function(_err, location) {
  
        let data = {
          cityName: location.city,
          zip: location.zip,
        }
        
        Forecast.app.models.Weather.forecast(data, function(_err, weatherResponse) {

          let customResponse = {
            location: {
              country: location.country,
              countryCode: location.countryCode,
              regionName: location.regionName,
              city: location.city,
              zip: location.zip,
              timezone: location.timezone
            },
            forecast: [],
          };
  
          let promises = [];

          weatherResponse.list.forEach(function (data) {
            promises.push(
              customResponse.forecast.push({
                day: data.dt_txt,
                main: data.weather[0].main,
                description: data.weather[0].description,
                temp: data.main.temp,
                feels_like: data.main.feels_like,
                temp_min: data.main.temp_min,
                temp_max: data.main.temp_max,
                visibility: data.visibility,
                wind: data.wind,
                clouds: data.clouds,
              })
            );
          });

          Promise.all(promises).then(() => {
            return callback(null, customResponse)
          });

        })  
      })
    }
  });

  Forecast.city = function (city, callback) {
    
    Forecast.app.models.Weather.forecast({ cityName: city }, function(_err, weatherResponse) {

      if (_err) {
        let error = new Error()
        if (_err && _err.status == '404' && _err.statusText == 'Not Found') {
          error.name = 'Bad Request';
          error.statusCode = 404;
          error.message = `No se encontró informació para la Ciudad "${city}"`
        } else {
          error.name = 'Internal error';
          error.statusCode = 500;
          error.message = 'Internal Server Error';          
        }
        return callback(error);

      } else {
  
        let customResponse = {
          location: { city: city },
          forecast: []
        };

        let promises = [];

        weatherResponse.list.forEach(function (data) {
          promises.push(
            customResponse.forecast.push({
              day: data.dt_txt,
              main: data.weather[0].main,
              description: data.weather[0].description,
              temp: data.main.temp,
              feels_like: data.main.feels_like,
              temp_min: data.main.temp_min,
              temp_max: data.main.temp_max,
              visibility: data.visibility,
              wind: data.wind,
              clouds: data.clouds,
            })
          );
        });

        Promise.all(promises).then(() => {
          return callback(null, customResponse)
        });

      }
    })
  }

  Forecast.remoteMethod('city',
    {
      accepts: [
        { arg: 'city', type: 'String', required: false, http: { source: 'path' }}
      ],
      http: { path: '/:city', verb: 'get' },
      returns: { type: 'Object', root: true }
    }
  )

};
