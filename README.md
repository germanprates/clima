# Clima

**Weather query app**

This application can show the current forecast, and for the next 5 days for the current city, it is also possible to consult a specific city by passing the name of the city in the url.

## Start API
at the root of the project you must first install the dependencies with `npm install`.

To launch the API, run the command
```
npm run start
```

the api will launch in `http://localhost:3000`


## Run Tests
to run the tests, run the command
```
npm run tests
```


## Available endpoints
To explore the available endpoints, in development mode you can refer to `http://localhost:3000/explorer` 


## Start in development mode
To start in development mode you must first install the development dependencies with `npm i -D`.

Then start the API with `npm run dev`

