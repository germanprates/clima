'use strict';

let supertest = require('supertest');
let should = require('should');

let serverUrl = require('./lib/server').serverUrl;
let server = supertest.agent(serverUrl);
const modelName = 'forecast';
const cityName = 'Buenos Aires';

console.log('serverUrl', serverUrl);

describe('Testing entrypoint: /' + modelName, function () {
  it('get ' + modelName + ' should return 200 with location and weather info', function (done) {
    server
      .get(modelName)
      .expect(200)
      .expect('Content-type', /json/)
      .end(function (err, resp) {
        if (err) {
          return done(err);
        }


        resp.body.should.have.property('location');
        resp.body.location.should.be.a.Object;

        resp.body.should.be.a.Object;
        resp.body.should.have.property('forecast');
        resp.body.forecast.should.be.a.Array;

        done();

      });
  });

  it( `get ${modelName}/${cityName} should return 200 with location and weather info`, function (done) {
    server
      .get(`${modelName}/${cityName}`)
      .expect(200)
      .expect('Content-type', /json/)
      .end(function (err, resp) {
        if (err) {
          return done(err);
        }


        resp.body.should.have.property('location');
        resp.body.location.should.be.a.Object;
        resp.body.location.city.should.be.equal(cityName); 

        resp.body.should.be.a.Object;
        resp.body.should.have.property('forecast');
        resp.body.forecast.should.be.a.Array;


        done();

      });
  });

  it( `get ${modelName}/failName should return 404 with error info`, function (done) {
    server
      .get(`${modelName}/failName`)
      .expect(404)
      .expect('Content-type', /json/)
      .end(function (err, resp) {
        if (err) return done(err);

        resp.body.should.have.property('error');
        resp.body.error.should.be.a.Object;
        resp.body.error.statusCode.should.be.equal(404);
        resp.body.error.name.should.be.equal('Bad Request');

        done();

      });
  });
});