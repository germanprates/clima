'use strict';
const axios = require('axios');
const CONFIG = require('../clima.config.json');
const IP_API_ADDRESS = CONFIG.services.ip_api.endpoint;
const DEF_RESP = CONFIG.services.ip_api.default.respose;
const DEF_FIELDS = CONFIG.services.ip_api.default.options;
const DEF_LANG = CONFIG.services.ip_api.default.lang;

module.exports = function(Ipapi) {

  Ipapi.getLocation = (ip, test, callback) => {

    if (test) return callback(null, DEF_RESP);
    let fields = DEF_FIELDS.join();
    let endpoint;
    if (ip !== undefined && ip !== null && ip !== '') {
      endpoint = `${IP_API_ADDRESS}/json/${ip}?fields=${fields}&lang=${DEF_LANG}`
    } else {
      endpoint = `${IP_API_ADDRESS}/json?fields=${fields}&lang=${DEF_LANG}`
    }

    axios.get(endpoint)
      .then(response => { 
        return callback(null, response.data)
      })
      .catch((error) => { 
        console.error('getLocation', error);
        return callback(null, DEF_RESP)
      });
  }
};
